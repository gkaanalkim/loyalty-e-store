package com.kgteknoloji.loyaltyestore.authorization;

import com.kgteknoloji.loyaltyestore.representation.entities.Authorization;
import com.kgteknoloji.loyaltyestore.representation.entities.RoleUser;
import com.kgteknoloji.loyaltyestore.representation.entities.Resource;
import com.kgteknoloji.loyaltyestore.representation.entities.UserToken;
import com.kgteknoloji.loyaltyestore.wandservice.WandResource;
import io.dropwizard.hibernate.HibernateBundle;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import javax.annotation.Priority;
import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.ext.Provider;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.security.Principal;

@WandAuthorization
@Provider
@Priority(Priorities.AUTHENTICATION)
public class WandAuthorizationFilter implements ContainerRequestFilter {
    @Context
    private ResourceInfo resourceInfo;

    HibernateBundle hibernateBundle;

    public WandAuthorizationFilter(HibernateBundle hibernateBundle) {
        this.hibernateBundle = hibernateBundle;
    }

    @Override
    public void filter(ContainerRequestContext requestContext) throws IOException {
        String authorizationHeader = requestContext.getHeaderString(HttpHeaders.AUTHORIZATION);

        if (authorizationHeader == null || !authorizationHeader.startsWith("Bearer")) {
            throw new NotAuthorizedException("Authorization header must be provided.");
        }

        String token = authorizationHeader.substring("Bearer".length()).trim();

        try {

            Session session = hibernateBundle.getSessionFactory().openSession();

            //  Find User
            final UserToken userToken = (UserToken) session.createCriteria(UserToken.class).add(Restrictions.eq("token", token)).uniqueResult();

            //  Find Role
            final RoleUser roleUser = (RoleUser) session.createCriteria(RoleUser.class).add(Restrictions.eq("user.oid", userToken.getUser().getOid())).uniqueResult();

            Annotation[] annotations = resourceInfo.getResourceMethod().getDeclaredAnnotations();

            String servicePath = null;

            for (Annotation annotation : annotations) {
                if (annotation instanceof WandResource) {
                    WandResource wandService = (WandResource) annotation;

                    servicePath = wandService.path();
                }
            }

            // Find Service
            final Resource service = (Resource) session.createCriteria(Resource.class).add(Restrictions.eq("path", servicePath)).uniqueResult();

            //  Find Service
            final Authorization authorization = (Authorization) session.createCriteria(Authorization.class)
                    .add(Restrictions.eq("role.oid", roleUser.getRole().getOid()))
                    .add(Restrictions.eq("resource.oid", service.getOid()))
                    .uniqueResult();

            if (authorization == null) {
                requestContext.abortWith(Response.status(Response.Status.UNAUTHORIZED).build());
            } else {
                requestContext.setSecurityContext(new SecurityContext() {
                    @Override
                    public Principal getUserPrincipal() {
                        return new Principal() {
                            @Override
                            public String getName() {
                                return userToken.getUser().getUsername();
                            }
                        };
                    }

                    @Override
                    public boolean isUserInRole(String role) {
                        return false;
                    }

                    @Override
                    public boolean isSecure() {
                        if (userToken != null)
                            return true;
                        else
                            return false;
                    }

                    @Override
                    public String getAuthenticationScheme() {
                        return null;
                    }

                    public String getToken() {
                        return null;
                    }
                });
            }

            session.close();

        } catch (Exception e) {
            System.out.println(e.getMessage());
            requestContext.abortWith(Response.status(Response.Status.UNAUTHORIZED).build());
        }
    }
}
