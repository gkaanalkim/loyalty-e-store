package com.kgteknoloji.loyaltyestore.authorization;

import io.dropwizard.ConfiguredBundle;
import io.dropwizard.hibernate.HibernateBundle;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;

public class WandAuthorizationBundle<T extends io.dropwizard.Configuration> implements ConfiguredBundle<T> {
    HibernateBundle hibernateBundle;

    public WandAuthorizationBundle(HibernateBundle hibernateBundle) {
        this.hibernateBundle = hibernateBundle;
    }

    @Override
    public void initialize(Bootstrap<?> bootstrap) {

    }

    @Override
    public void run(T t, Environment environment) throws Exception {
        environment.jersey().register(new WandAuthorizationFilter(hibernateBundle));
    }
}
