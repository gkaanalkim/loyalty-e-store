package com.kgteknoloji.loyaltyestore.authentication;

import com.kgteknoloji.loyaltyestore.representation.entities.UserToken;
import io.dropwizard.hibernate.HibernateBundle;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import javax.annotation.Priority;
import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.ext.Provider;
import java.io.IOException;
import java.security.Principal;

@WandAuthentication
@Provider
@Priority(Priorities.AUTHENTICATION)
public class WandAuthenticationFilter implements ContainerRequestFilter {
    HibernateBundle hibernateBundle;

    public WandAuthenticationFilter(HibernateBundle hibernateBundle) {
        this.hibernateBundle = hibernateBundle;
    }

    @Override
    public void filter(ContainerRequestContext requestContext) throws IOException {
        String authorizationHeader = requestContext.getHeaderString(HttpHeaders.AUTHORIZATION);

        if (authorizationHeader == null || !authorizationHeader.startsWith("Bearer")) {
            throw new NotAuthorizedException("Authorization header must be provided.");
        }

        String token = authorizationHeader.substring("Bearer".length()).trim();

        try {

            Session session = hibernateBundle.getSessionFactory().openSession();

            final UserToken userToken = (UserToken) session.createCriteria(UserToken.class).add(Restrictions.eq("token", token)).uniqueResult();

            if (userToken == null) {
                requestContext.abortWith(Response.status(Response.Status.UNAUTHORIZED).build());
            } else {
                requestContext.setSecurityContext(new SecurityContext() {
                    @Override
                    public Principal getUserPrincipal() {
                        return new Principal() {
                            @Override
                            public String getName() {
                                return userToken.getUser().getUsername();
                            }
                        };
                    }

                    @Override
                    public boolean isUserInRole(String role) {
                        return false;
                    }

                    @Override
                    public boolean isSecure() {
                        if (userToken != null)
                            return true;
                        else
                            return false;
                    }

                    @Override
                    public String getAuthenticationScheme() {
                        return null;
                    }

                    public String getToken() {
                        return null;
                    }
                });
            }

            session.close();

        } catch (Exception e) {
            requestContext.abortWith(Response.status(Response.Status.UNAUTHORIZED).build());
        }
    }
}
