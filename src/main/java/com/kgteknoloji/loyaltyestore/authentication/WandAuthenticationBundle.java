package com.kgteknoloji.loyaltyestore.authentication;

import io.dropwizard.Configuration;
import io.dropwizard.ConfiguredBundle;
import io.dropwizard.hibernate.HibernateBundle;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;

public class WandAuthenticationBundle<T extends Configuration> implements ConfiguredBundle<T> {
    HibernateBundle hibernateBundle;

    public WandAuthenticationBundle(HibernateBundle hibernateBundle) {
        this.hibernateBundle = hibernateBundle;
    }

    @Override
    public void run(T t, Environment environment) throws Exception {
        environment.jersey().register(new WandAuthenticationFilter(hibernateBundle));
    }

    @Override
    public void initialize(Bootstrap<?> bootstrap) {

    }
}
