package com.kgteknoloji.loyaltyestore.cli;

import com.kgteknoloji.loyaltyestore.BaseConfiguration;
import com.kgteknoloji.loyaltyestore.enums.MenuEnum;
import com.kgteknoloji.loyaltyestore.representation.entities.*;
import io.dropwizard.Application;
import io.dropwizard.Configuration;
import io.dropwizard.cli.EnvironmentCommand;
import io.dropwizard.hibernate.HibernateBundle;
import io.dropwizard.hibernate.UnitOfWork;
import io.dropwizard.setup.Environment;
import net.sourceforge.argparse4j.inf.Namespace;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by gunerkaanalkim on 10/01/18.
 */
public class InitializeCommand<T extends BaseConfiguration> extends EnvironmentCommand<T> {
    private static final Logger LOGGER = LoggerFactory.getLogger(InitializeCommand.class);
    private HibernateBundle hibernateBundle;

    public InitializeCommand(Application application, HibernateBundle hibernateBundle) {
        super(application, "initialize", "Runs initial command for initial process.");
        this.hibernateBundle = hibernateBundle;
    }

    @Override
    protected void run(Environment environment, Namespace namespace, T t) throws Exception {
        LOGGER.info("Initialize Starting...");
        LOGGER.info("Starting to create initial data.");
        execute(t);
    }

    @UnitOfWork
    public void execute(Configuration configuration) {
        final Session session = hibernateBundle.getSessionFactory().openSession();

        this.createMenu(session);
        this.createSuperUser(session);
        this.createProductAndPrices(session);
        this.createBalance(session);
        this.createTransactionType(session);
        this.createCategory(session);
        this.createAttribute(session);
        this.createProductAndCategoryAttribute(session);
        session.close();
    }

    public void createMenu(Session session) {
        int counter = 0;

        Menu userMainMenu = (Menu) session.createCriteria(Menu.class).add(Restrictions.eq("code", MenuEnum.SYSTEM_MANAGENEMENT.getMenuAlias())).uniqueResult();

        if (userMainMenu == null) {
            userMainMenu = new Menu();
            userMainMenu.setName("Sistem Yönetimi");
            userMainMenu.setCode(MenuEnum.SYSTEM_MANAGENEMENT.getMenuAlias());
            userMainMenu.setDescription("Sistem yönetimi sayfasıdır.");
            userMainMenu.setMenuOrder(counter);
            session.persist(userMainMenu);
        } else {
            userMainMenu.setName("Sistem Yönetimi");
            userMainMenu.setCode(MenuEnum.SYSTEM_MANAGENEMENT.getMenuAlias());
            userMainMenu.setDescription("Sistem yönetimi sayfasıdır.");
            userMainMenu.setMenuOrder(counter);
            session.persist(userMainMenu);
        }
        counter++;

        Menu authenticationAuthorization = (Menu) session.createCriteria(Menu.class).add(Restrictions.eq("code", MenuEnum.AUTHENICATION_AUTHORIZATION.getMenuAlias())).uniqueResult();

        if (authenticationAuthorization == null) {
            authenticationAuthorization = new Menu();
            authenticationAuthorization.setName("Kimliklendirme & Yetkilendirme");
            authenticationAuthorization.setCode(MenuEnum.AUTHENICATION_AUTHORIZATION.getMenuAlias());
            authenticationAuthorization.setDescription("Kullanıcı işlemleri sayfasıdır.");
            authenticationAuthorization.setUrl("kimliklendirme-yetkilendirme.html");
            authenticationAuthorization.setParentOid(userMainMenu.getOid());
            authenticationAuthorization.setMenuOrder(counter);
            session.persist(authenticationAuthorization);
        } else {
            authenticationAuthorization.setName("Kimliklendirme & Yetkilendirme");
            authenticationAuthorization.setCode(MenuEnum.AUTHENICATION_AUTHORIZATION.getMenuAlias());
            authenticationAuthorization.setDescription("Kullanıcı işlemleri sayfasıdır.");
            authenticationAuthorization.setUrl("kimliklendirme-yetkilendirme.html");
            authenticationAuthorization.setParentOid(userMainMenu.getOid());
            authenticationAuthorization.setMenuOrder(counter);
            session.persist(authenticationAuthorization);
        }
        counter++;

        Menu company = (Menu) session.createCriteria(Menu.class).add(Restrictions.eq("code", MenuEnum.COMPANY.getMenuAlias())).uniqueResult();

        if (company == null) {
            company = new Menu();
            company.setName("Firma Yönetimi");
            company.setCode(MenuEnum.COMPANY.getMenuAlias());
            company.setDescription("Firma oluşturma sayfasıdır.");
            company.setUrl("firma.html");
            company.setParentOid(userMainMenu.getOid());
            company.setMenuOrder(counter);
            session.persist(company);
        } else {
            company.setName("Firma Yönetimi");
            company.setCode(MenuEnum.COMPANY.getMenuAlias());
            company.setDescription("Firma oluşturma sayfasıdır.");
            company.setUrl("firma.html");
            company.setParentOid(userMainMenu.getOid());
            company.setMenuOrder(counter);
            session.persist(company);
        }

        session.flush();
    }

    public void createSuperUser(Session session) {
        User superUser = (User) session.createCriteria(User.class).add(Restrictions.eq("code", "SA")).uniqueResult();

        if (superUser == null) {
            superUser = new User();
            superUser.setName("Güner Kaan");
            superUser.setSurname("ALKIM");
            superUser.setCode("SA");
            superUser.setMobilePhone("05418847672");
            superUser.setInternalPhoneCode("0000");
            superUser.setUsername("admin");
            superUser.setPassword("03ac674216f3e15c761ee1a5e255f067953623c8b388b4459e13f978d7c846f4");

            session.persist(superUser);
        } else {
            superUser.setName("Güner Kaan");
            superUser.setSurname("ALKIM");
            superUser.setCode("SA");
            superUser.setMobilePhone("05418847672");
            superUser.setInternalPhoneCode("0000");
            superUser.setUsername("admin");
            superUser.setPassword("03ac674216f3e15c761ee1a5e255f067953623c8b388b4459e13f978d7c846f4");

            session.persist(superUser);
        }

        Role superAdminRole = (Role) session.createCriteria(Role.class).add(Restrictions.eq("code", "SAR")).uniqueResult();

        if (superAdminRole == null) {
            superAdminRole = new Role();
            superAdminRole.setCode("SAR");
            superAdminRole.setName("Super Admin Role");

            session.persist(superAdminRole);
        } else {
            superAdminRole.setCode("SAR");
            superAdminRole.setName(" Super Admin Role");

            session.persist(superAdminRole);
        }

        List<Resource> resources = session.createCriteria(Resource.class).list();

        for (Resource resource : resources) {
            Authorization authorization = (Authorization) session.createCriteria(Authorization.class)
                    .add(Restrictions.eq("role.oid", superAdminRole.getOid()))
                    .add(Restrictions.eq("resource.oid", resource.getOid()))
                    .uniqueResult();

            if (authorization == null) {
                authorization = new Authorization();
                authorization.setResource(resource);
                authorization.setRole(superAdminRole);

                session.persist(authorization);
            }
        }

        List<Menu> menus = session.createCriteria(Menu.class).list();

        for (Menu menu : menus) {
            RoleMenu roleMenu = (RoleMenu) session.createCriteria(RoleMenu.class)
                    .add(Restrictions.eq("role.oid", superAdminRole.getOid()))
                    .add(Restrictions.eq("menu.oid", menu.getOid()))
                    .uniqueResult();

            if (roleMenu == null) {
                roleMenu = new RoleMenu();
                roleMenu.setMenu(menu);
                roleMenu.setRole(superAdminRole);

                session.persist(roleMenu);
            }
        }

        RoleUser superAdminRoleUser = (RoleUser) session.createCriteria(RoleUser.class)
                .add(Restrictions.eq("role.oid", superAdminRole.getOid()))
                .add(Restrictions.eq("user.oid", superUser.getOid()))
                .uniqueResult();

        if (superAdminRoleUser == null) {
            superAdminRoleUser = new RoleUser();
            superAdminRoleUser.setRole(superAdminRole);
            superAdminRoleUser.setUser(superUser);

            session.persist(superAdminRoleUser);
        }


        session.flush();
    }

    public void createProductAndPrices(Session session) {
        /**
         * Sample Product
         * **/
        Product computer = (Product) session.createCriteria(Product.class).add(Restrictions.eq("sku", "bilgisayar_sku")).uniqueResult();

        if (computer == null) {
            computer = new Product();

            computer.setCode("COM");
            computer.setDescription("Bilgisayar");
            computer.setImagePaths("img1.png");
            computer.setSku("bilgisayar_sku");

            session.persist(computer);

            /**
             * Sample Currency
             * **/
            Currency currency = new Currency();
            currency.setCode("TL");
            currency.setName("Türk Lirası");

            session.persist(currency);

            Price price = new Price();
            price.setCurrency(currency);
            price.setName("Nakit Fiyatı");
            price.setProduct(computer);
            price.setValue(new BigDecimal(59.99));

            session.persist(price);
        }

        Product smartphone = (Product) session.createCriteria(Product.class).add(Restrictions.eq("sku", "smartphone_sku")).uniqueResult();

        if (smartphone == null) {
            smartphone = new Product();

            smartphone.setCode("SMR");
            smartphone.setDescription("Apple IPhone");
            smartphone.setImagePaths("img2.png");
            smartphone.setSku("smartphone_sku");

            session.persist(smartphone);

            /**
             * Sample Currency
             * **/
            Currency currency = new Currency();
            currency.setCode("TL");
            currency.setName("Türk Lirası");

            session.persist(currency);

            Price price = new Price();
            price.setCurrency(currency);
            price.setName("Nakit Fiyatı");
            price.setProduct(computer);
            price.setValue(new BigDecimal(99.99));

            session.persist(price);

        }

        session.flush();

    }

    public void createBalance(Session session) {
        User superUser = (User) session.createCriteria(User.class).add(Restrictions.eq("code", "SA")).uniqueResult();
        Balance balance = (Balance) session.createCriteria(Balance.class).add(Restrictions.eq("user.oid", superUser.getOid())).uniqueResult();

        if (superUser != null && balance == null) {
            balance = new Balance();
            balance.setUser(superUser);
            balance.setValue(new BigDecimal(100000));

            session.persist(balance);
        }

        session.flush();

    }

    public void createTransactionType(Session session) {
        TransactionType load = (TransactionType) session.createCriteria(TransactionType.class).add(Restrictions.eq("code", "LOA")).uniqueResult();
        TransactionType spend = (TransactionType) session.createCriteria(TransactionType.class).add(Restrictions.eq("code", "SPE")).uniqueResult();
        TransactionType zeroize = (TransactionType) session.createCriteria(TransactionType.class).add(Restrictions.eq("code", "ZER")).uniqueResult();
        TransactionType cancellationReturn = (TransactionType) session.createCriteria(TransactionType.class).add(Restrictions.eq("code", "CAN")).uniqueResult();

        if (load == null) {
            load = new TransactionType();
            load.setCode("LOA");
            load.setName("Loading - Yükleme");

            session.persist(load);
        }

        if (spend == null) {
            spend = new TransactionType();
            spend.setCode("SPE");
            spend.setName("Spend - Harcama");

            session.persist(spend);
        }

        if (zeroize == null) {
            zeroize = new TransactionType();
            zeroize.setCode("ZER");
            zeroize.setName("Zeroize - Sıfırlama");

            session.persist(zeroize);
        }

        if (cancellationReturn == null) {
            cancellationReturn = new TransactionType();
            cancellationReturn.setCode("CAN");
            cancellationReturn.setName("Cancellation Return - İptal İadesi");

            session.persist(cancellationReturn);
        }

        session.flush();
    }

    public void createCategory(Session session) {
        Category electronic = (Category) session.createCriteria(Category.class).add(Restrictions.eq("code", "ELC")).uniqueResult();

        if (electronic == null) {
            electronic = new Category();
            electronic.setCode("ELC");
            electronic.setName("Elektronik");
            electronic.setUrlPath("/elektronik");
            electronic.setParentOid(null);

            session.persist(electronic);
        }

        session.flush();
    }

    public void createAttribute(Session session) {
        Attribute foo = (Attribute) session.createCriteria(Attribute.class).add(Restrictions.eq("code", "foo")).uniqueResult();
        Attribute bar = (Attribute) session.createCriteria(Attribute.class).add(Restrictions.eq("code", "bar")).uniqueResult();
        Attribute tar = (Attribute) session.createCriteria(Attribute.class).add(Restrictions.eq("code", "tar")).uniqueResult();

        if (foo == null) {
            foo = new Attribute();
            foo.setCode("foo");
            foo.setName("foo");
            foo.setValue("foo");

            session.persist(foo);
        }

        if (bar == null) {
            bar = new Attribute();
            bar.setCode("bar");
            bar.setName("bar");
            bar.setValue("bar");

            session.persist(bar);
        }

        if (tar == null) {
            tar = new Attribute();
            tar.setCode("tar");
            tar.setName("tar");
            tar.setValue("tar");

            session.persist(tar);
        }

        session.flush();
    }

    public void createProductAndCategoryAttribute(Session session) {
        Attribute foo = (Attribute) session.createCriteria(Attribute.class).add(Restrictions.eq("code", "foo")).uniqueResult();
        Attribute bar = (Attribute) session.createCriteria(Attribute.class).add(Restrictions.eq("code", "bar")).uniqueResult();
        Attribute tar = (Attribute) session.createCriteria(Attribute.class).add(Restrictions.eq("code", "tar")).uniqueResult();

        Category electronic = (Category) session.createCriteria(Category.class).add(Restrictions.eq("code", "ELC")).uniqueResult();

        Product computer = (Product) session.createCriteria(Product.class).add(Restrictions.eq("sku", "bilgisayar_sku")).uniqueResult();
        Product smartphone = (Product) session.createCriteria(Product.class).add(Restrictions.eq("sku", "smartphone_sku")).uniqueResult();

        CategoryAttribute fooCategoryAttribute = (CategoryAttribute) session.createCriteria(CategoryAttribute.class)
                .add(Restrictions.eq("category.oid", electronic.getOid()))
                .add(Restrictions.eq("attribute.oid", foo.getOid())).uniqueResult();

        if (fooCategoryAttribute == null) {
            fooCategoryAttribute = new CategoryAttribute();
            fooCategoryAttribute.setAttribute(foo);
            fooCategoryAttribute.setCategory(electronic);

            session.persist(fooCategoryAttribute);
        }

        ProductAttribute fooProductAttribute = (ProductAttribute) session.createCriteria(ProductAttribute.class)
                .add(Restrictions.eq("product.oid", computer.getOid()))
                .add(Restrictions.eq("attribute.oid", foo.getOid())).uniqueResult();

        if (fooProductAttribute == null) {
            fooProductAttribute = new ProductAttribute();
            fooProductAttribute.setAttribute(foo);
            fooProductAttribute.setProduct(computer);

            session.persist(fooProductAttribute);
        }

        ProductAttribute fooProductAttribute2 = (ProductAttribute) session.createCriteria(ProductAttribute.class)
                .add(Restrictions.eq("product.oid", smartphone.getOid()))
                .add(Restrictions.eq("attribute.oid", foo.getOid())).uniqueResult();

        if (fooProductAttribute2 == null) {
            fooProductAttribute2 = new ProductAttribute();
            fooProductAttribute2.setAttribute(foo);
            fooProductAttribute2.setProduct(smartphone);

            session.persist(fooProductAttribute2);
        }

        session.flush();
    }


}
