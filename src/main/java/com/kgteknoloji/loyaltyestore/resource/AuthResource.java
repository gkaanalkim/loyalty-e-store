package com.kgteknoloji.loyaltyestore.resource;

import com.kgteknoloji.loyaltyestore.authentication.Credentials;
import com.kgteknoloji.loyaltyestore.authentication.TokenGenerator;
import com.kgteknoloji.loyaltyestore.authentication.WandAuthentication;
import com.kgteknoloji.loyaltyestore.dao.dao.UserDAO;
import com.kgteknoloji.loyaltyestore.dao.dao.UserTokenDAO;
import com.kgteknoloji.loyaltyestore.representation.dto.TokenDTO;
import com.kgteknoloji.loyaltyestore.representation.entities.User;
import com.kgteknoloji.loyaltyestore.representation.entities.UserToken;
import io.dropwizard.hibernate.UnitOfWork;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Map;

/**
 * Created by gunerkaanalkim on 09/09/15.
 */
@Path("auth")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class AuthResource {
    private static final Logger LOGGER = LoggerFactory.getLogger(AuthResource.class);

    UserDAO userDAO;
    UserTokenDAO userTokenDAO;

    public AuthResource(UserDAO userDAO, UserTokenDAO userTokenDAO) {
        this.userDAO = userDAO;
        this.userTokenDAO = userTokenDAO;
    }

    @POST
    @Path("login")
    @UnitOfWork
    public Response login(Credentials credentials) {
        User user = userDAO.findByUsername(credentials.getUsername());

        if (user == null) {
            LOGGER.error("Unknown User : {} ", credentials.toString());

            throw new WebApplicationException(Response.Status.UNAUTHORIZED);
        } else if (user.getPassword().equals(credentials.getPassword())) {
            UserToken userToken = userTokenDAO.getByUser(user);

            if (userToken != null) {
                userTokenDAO.delete(userToken);
            }

            TokenDTO tokenDTO = new TokenDTO(TokenGenerator.generate().toString(), 36000);

            userToken = new UserToken();
            userToken.setUser(user);
            userToken.setStatus(true);
            userToken.setToken(tokenDTO.getToken());

            userTokenDAO.create(userToken);
            LOGGER.info("LOGIN User : {} ", userToken.toString());

            return Response.ok().entity(tokenDTO).build();
        } else {
            LOGGER.error("Unauthorize User : {} ", credentials.toString());
            return Response.status(Response.Status.UNAUTHORIZED).build();
        }
    }

    @POST
    @Path("logout")
    @UnitOfWork
    @WandAuthentication
    public Response logout(Map<String, String> tokenData) {
        String token = tokenData.get("token");

        UserToken userToken = userTokenDAO.findByToken(token);
        userTokenDAO.delete(userToken);

        LOGGER.info("LOGOUT User : {} ", userToken.toString());

        return Response.ok().build();
    }
}
