package com.kgteknoloji.loyaltyestore.resource;

import com.kgteknoloji.loyaltyestore.authentication.WandAuthentication;
import com.kgteknoloji.loyaltyestore.authorization.WandAuthorization;
import com.kgteknoloji.loyaltyestore.dao.dao.*;
import com.kgteknoloji.loyaltyestore.representation.entities.*;
import com.kgteknoloji.loyaltyestore.wandservice.WandResource;
import io.dropwizard.hibernate.UnitOfWork;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Created by gunerkaanalkim on 19/09/15.
 */
@Path("menu")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class MenuResource {
    UserDAO userDAO;
    MenuDAO menuDAO;
    RoleDAO roleDAO;
    RoleMenuDAO roleMenuDAO;
    RoleUserDAO roleUserDAO;

    public MenuResource(UserDAO userDAO, MenuDAO menuDAO, RoleDAO roleDAO, RoleMenuDAO roleMenuDAO, RoleUserDAO roleUserDAO) {
        this.userDAO = userDAO;
        this.menuDAO = menuDAO;
        this.roleDAO = roleDAO;
        this.roleMenuDAO = roleMenuDAO;
        this.roleUserDAO = roleUserDAO;
    }

    @GET
    @Path("read")
    @UnitOfWork
    @WandAuthentication
    @WandAuthorization
    @WandResource(name = "menuRead", alias = "menuRead", description = "Tüm menü listesini görüntüleyebilir.", author = "Kaan ALKIM", path = "/menu/read", version = "1.0.0")
    public Response read(Map<String, String> menuData) {
        List<Menu> menus = menuDAO.getAll();

        return Response.ok(menus).build();
    }

    @GET
    @Path("readMenuByRole/{oid}")
    @UnitOfWork
    @WandAuthentication
    @WandAuthorization
    @WandResource(name = "readMenuByRole", alias = "readMenuByRole", description = "Bir rolün tüm menülerini görüntüleyebilir.", author = "Kaan ALKIM", path = "/menu/readMenuByRole/{oid}", version = "1.0.0")
    public Response readMenuByRole(@PathParam("oid") String oid) {
        Role role = roleDAO.findById(oid);

        List<RoleMenu> roleMenuList = roleMenuDAO.getByRole(role);

        List<Menu> menus = new LinkedList<>();

        for (RoleMenu roleMenu : roleMenuList) {
            menus.add(roleMenu.getMenu());
        }

        return Response.ok(menus).build();
    }

    @GET
    @Path("readMenuByUser")
    @UnitOfWork
    @WandAuthentication
    @WandAuthorization
    @WandResource(name = "readMenuByUser", alias = "readMenuByUser", description = "Bir kullanıcının tüm menülerini görüntüleyebilir.", author = "Kaan ALKIM", path = "/menu/readMenuByUser", version = "1.0.0")
    public Response readMenuByUser(Map<String, String> menuData, @Context SecurityContext securityContext) {
        User user = userDAO.findByUsername(securityContext.getUserPrincipal().getName());

        RoleUser roleUser = roleUserDAO.findByUser(user);
        List<RoleMenu> roleMenuList = roleMenuDAO.getByRole(roleUser.getRole());

        List<Menu> menus = new LinkedList<>();

        for (RoleMenu roleMenu : roleMenuList) {
            menus.add(roleMenu.getMenu());
        }

        return Response.ok(menus).build();
    }

    @POST
    @Path("assign")
    @UnitOfWork
    @WandAuthentication
    @WandAuthorization
    @WandResource(name = "assing", alias = "assing", description = "Bir rolün herhangi bir menüye erişim yetkisini verebilir.", author = "Kaan ALKIM", path = "/menu/assing", version = "1.0.0")
    public Response assing(Map<String, String> menuData) {
        String roleOid = menuData.get("role");
        Role role = roleDAO.findById(roleOid);

        String menuOid = menuData.get("menu");
        Menu menu = menuDAO.getById(menuOid);

        RoleMenu roleMenu = roleMenuDAO.getByRoleAndMenu(role, menu);

        if (roleMenu == null) {
            roleMenu = new RoleMenu();
            roleMenu.setMenu(menu);
            roleMenu.setRole(role);
            roleMenuDAO.create(roleMenu);

            return Response.ok(roleMenu).build();
        } else {
            roleMenu.setDeleteStatus(false);
            roleMenuDAO.update(roleMenu);

            return Response.ok(roleMenu).build();
        }
    }

    @POST
    @Path("unassign")
    @UnitOfWork
    @WandAuthentication
    @WandAuthorization
    @WandResource(name = "unassign", alias = "unassign", description = "Bir rolün herhangi bir menüye erişim yetkisini kaldırabilir.", author = "Kaan ALKIM", path = "/menu/unassign", version = "1.0.0")
    public Response unassign(Map<String, String> menuData) {
        String roleOid = menuData.get("role");
        Role role = roleDAO.findById(roleOid);

        String menuOid = menuData.get("menu");
        Menu menu = menuDAO.getById(menuOid);

        RoleMenu roleMenu = roleMenuDAO.getByRoleAndMenu(role, menu);
        roleMenuDAO.delete(roleMenu);

        return Response.ok(roleMenu).build();
    }

    @POST
    @Path("verifyMenu")
    @UnitOfWork
    @WandAuthentication
    @WandAuthorization
    @WandResource(name = "verifyMenu", alias = "verifyMenu", description = "Bir rolün herhangi bir menüye erişim yetkisinin olup olmadığının kontrolünün yetkisini verir.", author = "Kaan ALKIM", path = "/menu/verifyMenu", version = "1.0.0")
    public Response verifyMenu(Map<String, String> menuData, @Context SecurityContext securityContext) {
        User user = userDAO.findByUsername(securityContext.getUserPrincipal().getName());

        String menuCode = menuData.get("menuCode");

        RoleUser roleUser = roleUserDAO.findByUser(user);
        Menu menu = menuDAO.getByCode(menuCode);

        RoleMenu roleMenu = roleMenuDAO.getByRoleAndMenu(roleUser.getRole(), menu);

        return Response.ok(roleMenu.getMenu()).build();
    }

    @POST
    @Path("assignAllMenuToRole/{role}")
    @UnitOfWork
    @WandAuthentication
    @WandAuthorization
    @WandResource(name = "assignAllMenuToRole", alias = "assignAllMenuToRole", description = "Bir role tüm menüleri atama yetkisi verir.", author = "Kaan ALKIM", path = "assignAllMenuToRole/{role}", version = "1.0.0")
    public Response assignAllMenuToRole(@PathParam("role") String roleOid) {
        Role role = roleDAO.findById(roleOid);

        List<Menu> menuList = menuDAO.getAll(false);

        for (Menu menu : menuList) {

            RoleMenu roleMenu = roleMenuDAO.getByRoleAndMenu(role, menu);

            if (roleMenu == null) {
                roleMenu = new RoleMenu();

                roleMenu.setRole(role);
                roleMenu.setMenu(menu);

                roleMenuDAO.create(roleMenu);
            } else {
                roleMenuDAO.delete(roleMenu);
            }
        }

        return Response.ok(roleMenuDAO.getByRole(role)).build();
    }

    @POST
    @Path("unassignAllMenuFromRole/{role}")
    @UnitOfWork
    @WandAuthentication
    @WandAuthorization
    @WandResource(name = "unassignAllMenuToRole", alias = "unassignAllMenuToRole", description = "Bir rolden tüm menülere erişim yetkisinin kaldırımasını sağlayan yetkiyi verir.", author = "Kaan ALKIM", path = "unassignAllMenuFromRole/{role}", version = "1.0.0")
    public Response unassignAllMenuToRole(@PathParam("role") String roleOid) {
        Role role = roleDAO.findById(roleOid);
        List<RoleMenu> roleMenuList = roleMenuDAO.getByRole(role);

        for (RoleMenu roleMenu : roleMenuList) {
            roleMenuDAO.delete(roleMenu);
        }

        return Response.ok(roleMenuList).build();
    }
}
