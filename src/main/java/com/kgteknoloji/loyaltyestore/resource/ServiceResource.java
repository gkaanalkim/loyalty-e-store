package com.kgteknoloji.loyaltyestore.resource;

import com.kgteknoloji.loyaltyestore.authentication.WandAuthentication;
import com.kgteknoloji.loyaltyestore.authorization.WandAuthorization;
import com.kgteknoloji.loyaltyestore.dao.dao.AuthorizationDAO;
import com.kgteknoloji.loyaltyestore.dao.dao.ResourceDAO;
import com.kgteknoloji.loyaltyestore.dao.dao.RoleDAO;
import com.kgteknoloji.loyaltyestore.representation.entities.Authorization;
import com.kgteknoloji.loyaltyestore.representation.entities.Resource;
import com.kgteknoloji.loyaltyestore.representation.entities.Role;
import com.kgteknoloji.loyaltyestore.wandservice.WandResource;
import io.dropwizard.hibernate.UnitOfWork;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

@Path("resource")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class ServiceResource {
    ResourceDAO resourceDAO;
    RoleDAO roleDAO;
    AuthorizationDAO authorizationDAO;

    public ServiceResource(ResourceDAO resourceDAO, RoleDAO roleDAO, AuthorizationDAO authorizationDAO) {
        this.resourceDAO = resourceDAO;
        this.roleDAO = roleDAO;
        this.authorizationDAO = authorizationDAO;
    }

    @GET
    @Path("read")
    @UnitOfWork
    @WandAuthentication
    @WandAuthorization
    @WandResource(path = "/resource/read", alias = "/resource/read", description = "Tüm servisleri görüntüleyebilir.")
    public Response read() {
        return Response.ok().entity(resourceDAO.findAll()).build();
    }

    @GET
    @Path("read/{oid}")
    @UnitOfWork
    @WandAuthentication
    @WandAuthorization
    @WandResource(path = "/resource/read/{oid}", alias = "/resource/read/{oid}", description = "ID si eşleşen servisi görüntüleyebilir.")
    public Response read(@PathParam("oid") String oid) {
        return Response.ok().entity(resourceDAO.findByOid(oid)).build();
    }

    @GET
    @Path("readByRole/{roleOid}")
    @UnitOfWork
    @WandAuthentication
    @WandAuthorization
    @WandResource(path = "/resource/readByRole/{roleOid}", alias = "/resource/readByRole/{roleOid}", description = "Bir rolün tüm servislerini görüntüleyebilir.")
    public Response readByRole(@PathParam("roleOid") String oid) {
        Role role = roleDAO.findById(oid);

        List<Authorization> authorizations = authorizationDAO.getByRole(role);

        List<Resource> resources = new LinkedList<>();

        for (Authorization authorization : authorizations) {
            resources.add(authorization.getResource());
        }

        return Response.ok().entity(resources).build();
    }

    @POST
    @Path("assign")
    @UnitOfWork
    @WandAuthentication
    @WandAuthorization
    @WandResource(path = "/resource/assign", alias = "/resource/assign", description = "Atanan servise erişebilir.")
    public Response assign(Map<String, String> data) {
        Role role = roleDAO.findById(data.get("role"));
        Resource resource = resourceDAO.findByOid(data.get("resourceOid"));

        Authorization authorization = new Authorization();
        authorization.setResource(resource);
        authorization.setRole(role);

        authorizationDAO.create(authorization);

        return Response.ok().entity(authorization).build();
    }

    @POST
    @Path("unassign")
    @UnitOfWork
    @WandAuthentication
    @WandAuthorization
    @WandResource(path = "/resource/unassign", alias = "/resource/unassign", description = "Atanan servise erişebilir.")
    public Response unassign(Map<String, String> data) {
        Role role = roleDAO.findById(data.get("role"));
        Resource resource = resourceDAO.findByOid(data.get("resourceOid"));

        Authorization authorization = authorizationDAO.findByRoleAndResource(role, resource);
        authorizationDAO.delete(authorization);

        return Response.ok().entity(authorization).build();
    }

    @POST
    @Path("assignAllResource/{roleOid}")
    @UnitOfWork
    @WandAuthentication
    @WandAuthorization
    @WandResource(path = "/resource/assignAllResource/{roleOid}", alias = "/resource/assignAllResource/{roleOid}", description = "Tüm servislere erişebilir.")
    public Response assignAllResource(@PathParam("roleOid") String roleOid) {
        List<Resource> resources = resourceDAO.findAll();
        Role role = roleDAO.getById(roleOid);

        for (Resource resource : resources) {
            Authorization authorization = new Authorization();
            authorization.setRole(role);
            authorization.setResource(resource);
            authorizationDAO.create(authorization);
        }

        return Response.ok().entity(resources).build();
    }


    @POST
    @Path("unassignAllResource/{roleOid}")
    @UnitOfWork
    @WandAuthentication
    @WandAuthorization
    @WandResource(path = "/resource/unassignAllResource/{roleOid}", alias = "/resource/unassignAllResource/{roleOid}", description = "Hiç bir servise erişimez.")
    public Response unassignAllResource(@PathParam("roleOid") String roleOid) {
        Role role = roleDAO.getById(roleOid);

        List<Authorization> authorizations = authorizationDAO.getByRole(role);

        for (Authorization authorization : authorizations) {
            authorizationDAO.delete(authorization);
        }

        return Response.ok().entity(authorizations).build();
    }
}
