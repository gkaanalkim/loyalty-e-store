package com.kgteknoloji.loyaltyestore.resource;

import com.kgteknoloji.loyaltyestore.authentication.WandAuthentication;
import com.kgteknoloji.loyaltyestore.authorization.WandAuthorization;
import com.kgteknoloji.loyaltyestore.dao.dao.BrandDAO;
import com.kgteknoloji.loyaltyestore.representation.entities.Brand;
import com.kgteknoloji.loyaltyestore.wandservice.WandResource;
import io.dropwizard.hibernate.UnitOfWork;

import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("brand")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class BrandResource {

    BrandDAO brandDAO;

    public BrandResource(BrandDAO brandDAO) {
        this.brandDAO = brandDAO;
    }

    @POST
    @UnitOfWork
    @WandAuthentication
    @WandAuthorization
    @WandResource(name = "createBrand", alias = "createBrand", description = "Marka oluşturabilir.", author = "Kaan ALKIM", path = "/brand", version = "1.0.0")
    public Response create(@Valid Brand brand) {
        return Response.ok(brandDAO.create(brand)).build();
    }

    @GET
    @UnitOfWork
    public Response read() {
        return Response.ok(brandDAO.findAll(Brand.class)).build();
    }

    @GET
    @Path("/{oid}")
    @UnitOfWork
    public Response readByOid(@PathParam("oid") String oid) {
        return Response.ok(brandDAO.findByOid(Brand.class, oid)).build();
    }

    @PUT
    @UnitOfWork
    @WandAuthentication
    @WandAuthorization
    @WandResource(name = "updateBrand", alias = "updateBrand", description = "Bir marka güncelleyebilir.", author = "Kaan ALKIM", path = "/brand", version = "1.0.0")
    public Response update(@Valid Brand brand) {
        return Response.ok(brandDAO.update(brand)).build();
    }

    @DELETE
    @UnitOfWork
    @WandAuthentication
    @WandAuthorization
    @WandResource(name = "deleteBrand", alias = "deleteBrand", description = "Bir marka silebilir.", author = "Kaan ALKIM", path = "/brand", version = "1.0.0")
    public Response delete(@Valid Brand brand) {
        return Response.ok(brandDAO.delete(Brand.class, brand)).build();
    }

}
