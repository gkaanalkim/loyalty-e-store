package com.kgteknoloji.loyaltyestore.resource;

import com.kgteknoloji.loyaltyestore.authentication.WandAuthentication;
import com.kgteknoloji.loyaltyestore.authorization.WandAuthorization;
import com.kgteknoloji.loyaltyestore.dao.dao.PriceDAO;
import com.kgteknoloji.loyaltyestore.representation.entities.Price;
import com.kgteknoloji.loyaltyestore.wandservice.WandResource;
import io.dropwizard.hibernate.UnitOfWork;

import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("price")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class PriceResource {
    PriceDAO priceDAO;

    public PriceResource(PriceDAO priceDAO) {
        this.priceDAO = priceDAO;
    }

    @POST
    @UnitOfWork
    @WandAuthentication
    @WandAuthorization
    @WandResource(name = "createPrice", alias = "createPrice", description = "Ürün fiyatı oluşturabilir.", author = "Kaan ALKIM", path = "/price", version = "1.0.0")
    public Response create(@Valid Price price) {
        return Response.ok(priceDAO.create(price)).build();
    }

    @GET
    @UnitOfWork
    public Response read() {
        return Response.ok(priceDAO.findAll(Price.class)).build();
    }

    @GET
    @Path("/{oid}")
    @UnitOfWork
    public Response readByOid(@PathParam("oid") String oid) {
        return Response.ok(priceDAO.findByOid(Price.class, oid)).build();
    }

    @PUT
    @UnitOfWork
    @WandAuthentication
    @WandAuthorization
    @WandResource(name = "updatePrice", alias = "updatePrice", description = "Ürün fiyatı güncelleyebilir.", author = "Kaan ALKIM", path = "/price", version = "1.0.0")
    public Response update(@Valid  Price price) {
        return Response.ok(priceDAO.update(price)).build();
    }

    @DELETE
    @UnitOfWork
    @WandAuthentication
    @WandAuthorization
    @WandResource(name = "updatePrice", alias = "updatePrice", description = "Ürün fiyatı silebilir.", author = "Kaan ALKIM", path = "/price", version = "1.0.0")
    public Response delete(@Valid  Price price) {
        return Response.ok(priceDAO.delete(Price.class, price)).build();
    }
}
