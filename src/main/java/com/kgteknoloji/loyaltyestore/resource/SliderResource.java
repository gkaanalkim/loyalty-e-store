package com.kgteknoloji.loyaltyestore.resource;

import com.kgteknoloji.loyaltyestore.authentication.WandAuthentication;
import com.kgteknoloji.loyaltyestore.authorization.WandAuthorization;
import com.kgteknoloji.loyaltyestore.dao.dao.SliderDAO;
import com.kgteknoloji.loyaltyestore.representation.entities.Slider;
import com.kgteknoloji.loyaltyestore.wandservice.WandResource;
import io.dropwizard.hibernate.UnitOfWork;

import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;

@Path("slider")
public class SliderResource {
    SliderDAO sliderDAO;

    public SliderResource(SliderDAO sliderDAO) {
        this.sliderDAO = sliderDAO;
    }

    @POST
    @UnitOfWork
    @WandAuthentication
    @WandAuthorization
    @WandResource(name = "uploadSlider", alias = "uploadSlider", description = "Slider görseli yükleyebilir.", author = "Kaan ALKIM", path = "/slider/uploadSlider", version = "1.0.0")
    public Response create(@Valid Slider slider) {
        return Response.ok(sliderDAO.create(slider)).build();
    }

    @GET
    @Path("/{pageName}")
    @UnitOfWork
    public Response readByOid(@PathParam("pageName") String pageName) {
        return Response.ok(sliderDAO.findByPageName(pageName)).build();
    }

    @PUT
    @UnitOfWork
    @WandAuthentication
    @WandAuthorization
    @WandResource(name = "updateSlider", alias = "updateSlider", description = "Bir slider güncelleyebilir.", author = "Kaan ALKIM", path = "/slider/updateSlider", version = "1.0.0")
    public Response update(@Valid Slider slider) {
        return Response.ok(sliderDAO.update(slider)).build();
    }

    @DELETE
    @UnitOfWork
    @WandAuthentication
    @WandAuthorization
    @WandResource(name = "deleteSlider", alias = "deleteSlider", description = "Bir slider silebilir.", author = "Kaan ALKIM", path = "/slider/updateSlider", version = "1.0.0")
    public Response delete(@Valid Slider slider) {
        return Response.ok(sliderDAO.delete(Slider.class, slider)).build();
    }

}
