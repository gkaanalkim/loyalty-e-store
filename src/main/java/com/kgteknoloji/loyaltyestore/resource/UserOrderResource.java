package com.kgteknoloji.loyaltyestore.resource;

import com.kgteknoloji.loyaltyestore.authentication.WandAuthentication;
import com.kgteknoloji.loyaltyestore.dao.dao.*;
import com.kgteknoloji.loyaltyestore.representation.dto.OrderDTO;
import com.kgteknoloji.loyaltyestore.representation.entities.*;
import com.kgteknoloji.loyaltyestore.wandservice.WandResource;
import io.dropwizard.hibernate.UnitOfWork;

import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import java.math.BigDecimal;

@Path("order")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class UserOrderResource {
    UserDAO userDAO;
    ProductDAO productDAO;
    ProductVariationDAO productVariationDAO;
    UserOrderDAO userOrderDAO;
    PriceDAO priceDAO;
    BalanceDAO balanceDAO;
    TransactionDAO transactionDAO;
    TransactionTypeDAO transactionTypeDAO;

    public UserOrderResource(UserDAO userDAO, ProductDAO productDAO, ProductVariationDAO productVariationDAO, UserOrderDAO userOrderDAO, PriceDAO priceDAO, BalanceDAO balanceDAO, TransactionDAO transactionDAO, TransactionTypeDAO transactionTypeDAO) {
        this.userDAO = userDAO;
        this.productDAO = productDAO;
        this.productVariationDAO = productVariationDAO;
        this.userOrderDAO = userOrderDAO;
        this.priceDAO = priceDAO;
        this.balanceDAO = balanceDAO;
        this.transactionDAO = transactionDAO;
        this.transactionTypeDAO = transactionTypeDAO;
    }

    @POST
    @UnitOfWork
    @WandAuthentication
    @WandResource(name = "createOrder", alias = "createOrder", description = "Sipariş oluşturabilir.", author = "Kaan ALKIM", path = "/order/createOrder", version = "1.0.0")
    public Response create(@Valid OrderDTO orderDTO, @Context SecurityContext securityContext) {
        User user = userDAO.findByUsername(securityContext.getUserPrincipal().getName());
        Product product = productDAO.findByOid(Product.class, orderDTO.getProductOid());
        ProductVariation productVariation = productVariationDAO.findByOid(ProductVariation.class, orderDTO.getProductVariationOid());
        Price price = priceDAO.findByOid(Price.class, orderDTO.getPriceOid());
        Balance balance = balanceDAO.findByUser(user);

        /**
         * Balance Check
         * */
        Double totalPrice = price.getValue().doubleValue() * orderDTO.getQuantity();

        if ((balance.getValue().doubleValue() >= totalPrice)) {
            UserOrder userOrder = new UserOrder();
            userOrder.setPrice(price);
            userOrder.setProduct(product);
            userOrder.setProductVariation(productVariation);
            userOrder.setQuantity(orderDTO.getQuantity());
            userOrder.setUser(user);
            userOrder.setTotalPrice(new BigDecimal(totalPrice));

            userOrderDAO.create(userOrder);

            /**
             * User balance update
             * **/
            BigDecimal newBalance = new BigDecimal(balance.getValue().doubleValue() - totalPrice);
            balance.setValue(newBalance);
            balanceDAO.update(balance);

            /**
             * Transaction Creation
             * **/
            TransactionType transactionType = transactionTypeDAO.findByCode("SPE");

            Transaction transaction = new Transaction();
            transaction.setTransactionType(transactionType);
            transaction.setUser(user);
            transaction.setValue(new BigDecimal(totalPrice));
            transactionDAO.create(transaction);

            return Response.ok(userOrder).build();
        } else {
            return Response.serverError().entity("Yetersiz Bakiye").build();
        }
    }
}
