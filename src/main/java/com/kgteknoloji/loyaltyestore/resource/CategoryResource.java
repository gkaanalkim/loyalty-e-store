package com.kgteknoloji.loyaltyestore.resource;


import com.kgteknoloji.loyaltyestore.authentication.WandAuthentication;
import com.kgteknoloji.loyaltyestore.authorization.WandAuthorization;
import com.kgteknoloji.loyaltyestore.dao.dao.CategoryDAO;
import com.kgteknoloji.loyaltyestore.dao.dao.ProductDAO;
import com.kgteknoloji.loyaltyestore.representation.entities.Category;
import com.kgteknoloji.loyaltyestore.representation.entities.Product;
import com.kgteknoloji.loyaltyestore.wandservice.WandResource;
import io.dropwizard.hibernate.UnitOfWork;

import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("category")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class CategoryResource {

    CategoryDAO categoryDAO;
    ProductDAO productDAO;

    public CategoryResource(CategoryDAO categoryDAO, ProductDAO productDAO) {
        this.categoryDAO = categoryDAO;
        this.productDAO = productDAO;
    }

    @POST
    @UnitOfWork
    @WandAuthentication
    @WandAuthorization
    @WandResource(name = "createCategory", alias = "createCategory", description = "Kategori oluşturabilir.", author = "Kaan ALKIM", path = "/category", version = "1.0.0")
    public Response create(@Valid Category category) {
        return Response.ok(categoryDAO.create(category)).build();
    }

    @GET
    @UnitOfWork
    public Response read() {
        return Response.ok(categoryDAO.findAll(Category.class)).build();
    }

    @GET
    @Path("/{oid}")
    @UnitOfWork
    public Response readByOid(@PathParam("oid") String oid) {
        return Response.ok(categoryDAO.findByOid(Category.class, oid)).build();
    }

    @GET
    @Path("/{oid}/product")
    @UnitOfWork
    public Response readProduct(@PathParam("oid") String oid) {
        Category category = categoryDAO.findByOid(Category.class, oid);
        List<Product> products = productDAO.findByCategory(category);

        return Response.ok(products).build();
    }

    @PUT
    @UnitOfWork
    @WandAuthentication
    @WandAuthorization
    @WandResource(name = "updateCategory", alias = "updateCategory", description = "Bir kategori güncelleyebilir.", author = "Kaan ALKIM", path = "/category", version = "1.0.0")
    public Response update(@Valid Category category) {
        return Response.ok(categoryDAO.update(category)).build();
    }

    @DELETE
    @UnitOfWork
    @WandAuthentication
    @WandAuthorization
    @WandResource(name = "deleteCategory", alias = "deleteCategory", description = "Bir kategori silebilir.", author = "Kaan ALKIM", path = "/category", version = "1.0.0")
    public Response delete(@Valid Category category) {
        return Response.ok(categoryDAO.delete(Category.class, category)).build();
    }

}
