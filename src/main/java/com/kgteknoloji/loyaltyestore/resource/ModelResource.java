package com.kgteknoloji.loyaltyestore.resource;

import com.kgteknoloji.loyaltyestore.authentication.WandAuthentication;
import com.kgteknoloji.loyaltyestore.authorization.WandAuthorization;
import com.kgteknoloji.loyaltyestore.dao.dao.ModelDAO;
import com.kgteknoloji.loyaltyestore.representation.entities.Model;
import com.kgteknoloji.loyaltyestore.wandservice.WandResource;
import io.dropwizard.hibernate.UnitOfWork;

import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("model")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class ModelResource {

    ModelDAO modelDAO;

    public ModelResource(ModelDAO modelDAO) {
        this.modelDAO = modelDAO;
    }

    @POST
    @UnitOfWork
    @WandAuthentication
    @WandAuthorization
    @WandResource(name = "createModel", alias = "createModel", description = "Model oluşturabilir.", author = "Kaan ALKIM", path = "/model", version = "1.0.0")
    public Response create(@Valid Model model) {
        return Response.ok(modelDAO.create(model)).build();
    }

    @GET
    @UnitOfWork
    public Response read() {
        return Response.ok(modelDAO.findAll(Model.class)).build();
    }

    @GET
    @Path("/{oid}")
    @UnitOfWork
    public Response readByOid(@PathParam("oid") String oid) {
        return Response.ok(modelDAO.findByOid(Model.class, oid)).build();
    }

    @PUT
    @UnitOfWork
    @WandAuthentication
    @WandAuthorization
    @WandResource(name = "updateModel", alias = "updateModel", description = "Model güncelleyebilir.", author = "Kaan ALKIM", path = "/model", version = "1.0.0")
    public Response update(@Valid Model model) {
        return Response.ok(modelDAO.update(model)).build();
    }

    @DELETE
    @UnitOfWork
    @WandAuthentication
    @WandAuthorization
    @WandResource(name = "deleteModel", alias = "deleteModel", description = "Model silebilir.", author = "Kaan ALKIM", path = "/model", version = "1.0.0")
    public Response delete(@Valid Model model) {
        return Response.ok(modelDAO.delete(Model.class, model)).build();
    }

}