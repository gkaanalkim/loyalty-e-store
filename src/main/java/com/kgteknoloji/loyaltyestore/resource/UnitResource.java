package com.kgteknoloji.loyaltyestore.resource;

import com.kgteknoloji.loyaltyestore.authentication.WandAuthentication;
import com.kgteknoloji.loyaltyestore.authorization.WandAuthorization;
import com.kgteknoloji.loyaltyestore.dao.dao.UnitDAO;
import com.kgteknoloji.loyaltyestore.representation.entities.Unit;
import com.kgteknoloji.loyaltyestore.wandservice.WandResource;
import io.dropwizard.hibernate.UnitOfWork;

import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("unit")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class UnitResource {

    UnitDAO unitDAO;

    public UnitResource(UnitDAO unitDAO) {
        this.unitDAO = unitDAO;
    }

    @POST
    @UnitOfWork
    @WandAuthentication
    @WandAuthorization
    @WandResource(name = "createUnit", alias = "createUnit", description = "Birim oluşturabilir.", author = "Kaan ALKIM", path = "/unit", version = "1.0.0")
    public Response create(@Valid Unit unit) {
        return Response.ok(unitDAO.create(unit)).build();
    }

    @GET
    @UnitOfWork
    public Response read() {
        return Response.ok(unitDAO.findAll(Unit.class)).build();
    }

    @GET
    @Path("/{oid}")
    @UnitOfWork
    public Response readByOid(@PathParam("oid") String oid) {
        return Response.ok(unitDAO.findByOid(Unit.class, oid)).build();
    }

    @PUT
    @UnitOfWork
    @WandAuthentication
    @WandAuthorization
    @WandResource(name = "updateUnit", alias = "updateUnit", description = "Bir birim güncelleyebilir.", author = "Kaan ALKIM", path = "/unit", version = "1.0.0")
    public Response update(@Valid Unit unit) {
        return Response.ok(unitDAO.update(unit)).build();
    }

    @DELETE
    @UnitOfWork
    @WandAuthentication
    @WandAuthorization
    @WandResource(name = "deleteUnit", alias = "deleteUnit", description = "Bir birim silebilir.", author = "Kaan ALKIM", path = "/unit", version = "1.0.0")
    public Response delete(@Valid Unit unit) {
        return Response.ok(unitDAO.delete(Unit.class, unit)).build();
    }

}
