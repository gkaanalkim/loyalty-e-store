package com.kgteknoloji.loyaltyestore.resource;

import com.kgteknoloji.loyaltyestore.authentication.WandAuthentication;
import com.kgteknoloji.loyaltyestore.authorization.WandAuthorization;
import com.kgteknoloji.loyaltyestore.dao.dao.*;
import com.kgteknoloji.loyaltyestore.representation.dto.ProductDTO;
import com.kgteknoloji.loyaltyestore.representation.entities.*;
import com.kgteknoloji.loyaltyestore.wandservice.WandResource;
import io.dropwizard.hibernate.UnitOfWork;

import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Path("product")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class ProductResource {
    ProductDAO productDAO;
    ProductAttributeDAO productAttributeDAO;
    ProductVariationDAO productVariationDAO;
    PriceDAO priceDAO;
    AttributeDAO attributeDAO;
    CategoryAttributeDAO categoryAttributeDAO;

    public ProductResource(ProductDAO productDAO, ProductAttributeDAO productAttributeDAO, ProductVariationDAO productVariationDAO, PriceDAO priceDAO, AttributeDAO attributeDAO, CategoryAttributeDAO categoryAttributeDAO) {
        this.productDAO = productDAO;
        this.productAttributeDAO = productAttributeDAO;
        this.productVariationDAO = productVariationDAO;
        this.priceDAO = priceDAO;
        this.attributeDAO = attributeDAO;
        this.categoryAttributeDAO = categoryAttributeDAO;
    }

    @POST
    @UnitOfWork
    @WandAuthentication
    @WandAuthorization
    @WandResource(name = "createProduct", alias = "createProduct", description = "Ürün oluşturabilir.", author = "Kaan ALKIM", path = "/product", version = "1.0.0")
    public Response create(@Valid Product product) {
        return Response.ok(productDAO.create(product)).build();
    }

    @GET
    @UnitOfWork
    public Response read() {
        return Response.ok(productDAO.findAll(Product.class)).build();
    }

    @GET
    @Path("/{oid}")
    @UnitOfWork
    public Response readByOid(@PathParam("oid") String oid) {
        return Response.ok(productDAO.findByOid(Product.class, oid)).build();
    }

    @GET
    @Path("/{oid}/price")
    @UnitOfWork
    public Response readProductPrice(@PathParam("oid") String oid) {
        Product product = productDAO.findByOid(Product.class, oid);
        List<Price> prices = priceDAO.findByProduct(product);

        return Response.ok(prices).build();
    }

    @GET
    @Path("/{oid}/attribute")
    @UnitOfWork
    public Response readProductAttribute(@PathParam("oid") String oid) {
        Product product = productDAO.findByOid(Product.class, oid);
        List<ProductAttribute> productAttributes = productAttributeDAO.findByProduct(product);

        return Response.ok(productAttributes).build();
    }

    @GET
    @Path("/{oid}/variation")
    @UnitOfWork
    public Response readProductVariation(@PathParam("oid") String oid) {
        Product product = productDAO.findByOid(Product.class, oid);
        List<ProductVariation> productVariations = productVariationDAO.findByProduct(product);

        return Response.ok(productVariations).build();
    }


    @GET
    @Path("/{oid}/detail")
    @UnitOfWork
    public Response detail(@PathParam("oid") String oid) {
        Product product = productDAO.findByOid(Product.class, oid);
        List<ProductVariation> productVariations = productVariationDAO.findByProduct(product);
        List<ProductAttribute> productAttributes = productAttributeDAO.findByProduct(product);
        List<Price> prices = priceDAO.findByProduct(product);

        return Response.ok(new ProductDTO(product, prices, productAttributes, productVariations)).build();
    }

    @PUT
    @UnitOfWork
    @WandAuthentication
    @WandAuthorization
    @WandResource(name = "updateProduct", alias = "updateProduct", description = "Ürün güncelleyebilir.", author = "Kaan ALKIM", path = "/product", version = "1.0.0")
    public Response update(@Valid Product product) {
        return Response.ok(productDAO.update(product)).build();
    }

    @DELETE
    @UnitOfWork
    @WandAuthentication
    @WandAuthorization
    @WandResource(name = "updatePrice", alias = "updatePrice", description = "Ürün silebilir.", author = "Kaan ALKIM", path = "/product", version = "1.0.0")
    public Response delete(@Valid Product product) {
        return Response.ok(productDAO.delete(Product.class, product)).build();
    }

    @GET
    @Path("/filter")
    @UnitOfWork
    public Response filter(@Context UriInfo uriInfo) {
        MultivaluedMap<String, String> queryParams = uriInfo.getQueryParameters();
        Map<Attribute, String> attributeStringMap = new HashMap<>();

        for (String attributeCode : queryParams.keySet()) {
            Attribute attribute = attributeDAO.findByCode(attributeCode);

            if (attribute != null && !queryParams.get(attributeCode).get(0).equals("")) {
                attributeStringMap.put(attribute, queryParams.get(attributeCode).get(0));
            }
        }

        List<Product> products = productAttributeDAO.findByAttributes(attributeStringMap);

        return Response.ok().entity(products).build();
    }

}
