package com.kgteknoloji.loyaltyestore.resource;

import com.kgteknoloji.loyaltyestore.authentication.WandAuthentication;
import com.kgteknoloji.loyaltyestore.authorization.WandAuthorization;
import com.kgteknoloji.loyaltyestore.dao.dao.CurrencyDAO;
import com.kgteknoloji.loyaltyestore.representation.entities.Currency;
import com.kgteknoloji.loyaltyestore.wandservice.WandResource;
import io.dropwizard.hibernate.UnitOfWork;

import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("currency")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class CurrencyResource {

    CurrencyDAO currencyDAO;

    public CurrencyResource(CurrencyDAO currencyDAO) {
        this.currencyDAO = currencyDAO;
    }

    @POST
    @UnitOfWork
    @WandAuthentication
    @WandAuthorization
    @WandResource(name = "createCurrency", alias = "createCurrency", description = "Para birimi oluşturabilir.", author = "Kaan ALKIM", path = "/currency", version = "1.0.0")
    public Response create(@Valid Currency currency) {
        return Response.ok(currencyDAO.create(currency)).build();
    }

    @GET
    @UnitOfWork
    public Response read() {
        return Response.ok(currencyDAO.findAll(Currency.class)).build();
    }

    @GET
    @Path("/{oid}")
    @UnitOfWork
    public Response readByOid(@PathParam("oid") String oid) {
        return Response.ok(currencyDAO.findByOid(Currency.class, oid)).build();
    }

    @PUT
    @UnitOfWork
    @WandAuthentication
    @WandAuthorization
    @WandResource(name = "updateCurrency", alias = "updateCurrency", description = "Para birimi güncelleyebilir.", author = "Kaan ALKIM", path = "/currency", version = "1.0.0")
    public Response update(@Valid Currency currency) {
        return Response.ok(currencyDAO.update(currency)).build();
    }

    @DELETE
    @UnitOfWork
    @WandAuthentication
    @WandAuthorization
    @WandResource(name = "deleteCurrency", alias = "deleteCurrency", description = "Bir para birimi silebilir.", author = "Kaan ALKIM", path = "/currency", version = "1.0.0")
    public Response delete(@Valid Currency currency) {
        return Response.ok(currencyDAO.delete(Currency.class, currency)).build();
    }

}