package com.kgteknoloji.loyaltyestore.dao.core;

import com.kgteknoloji.loyaltyestore.representation.core.BaseEntity;
import io.dropwizard.hibernate.AbstractDAO;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;

import java.util.List;

public class SoftDAO<T extends BaseEntity> extends AbstractDAO<T> {
    public SoftDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public List<T> findAll(Class<T> t) {
        Criteria criteria = currentSession().createCriteria(t);
        criteria.add(Restrictions.eq("deleteStatus", false));
        return list(criteria);
    }

    public T create(T entity) {
        return persist(entity);
    }

    public T findByOid(Class<T> t, String oid) {
        Criteria criteria = currentSession().createCriteria(t);
        criteria.add(Restrictions.eq("deleteStatus", false));
        criteria.add(Restrictions.eq("oid", oid));
        return uniqueResult(criteria);
    }

    public T update(T entity) {
        return persist(entity);
    }

    public T delete(Class<T> t, T entity) {
        Criteria criteria = currentSession().createCriteria(t);
        criteria.add(Restrictions.eq("deleteStatus", false));
        criteria.add(Restrictions.eq("oid", entity.getOid()));

        T oldEntity = uniqueResult(criteria);
        oldEntity.setDeleteStatus(true);

        return this.update(oldEntity);
    }

    public void flush() {
        currentSession().flush();
    }

    public T merge(T entity) {
        return (T) currentSession().merge(entity);
    }

    public T detach(T entity) {
        currentSession().evict(entity);
        return entity;
    }
}
