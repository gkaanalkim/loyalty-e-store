package com.kgteknoloji.loyaltyestore.dao.dao;

import com.kgteknoloji.loyaltyestore.dao.core.SoftDAO;
import com.kgteknoloji.loyaltyestore.representation.entities.Slider;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;

public class SliderDAO extends SoftDAO<Slider> {
    public SliderDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public Slider findByPageName(String pageName) {
        Criteria criteria = currentSession().createCriteria(Slider.class);
        criteria.add(Restrictions.eq("deleteStatus", false));
        criteria.add(Restrictions.eq("pageName", pageName));

        return uniqueResult(criteria);
    }
}
