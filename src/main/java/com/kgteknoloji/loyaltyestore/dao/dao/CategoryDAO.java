package com.kgteknoloji.loyaltyestore.dao.dao;

import com.kgteknoloji.loyaltyestore.dao.core.SoftDAO;
import com.kgteknoloji.loyaltyestore.representation.entities.Category;
import org.hibernate.SessionFactory;

public class CategoryDAO extends SoftDAO<Category> {
    public CategoryDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

}
