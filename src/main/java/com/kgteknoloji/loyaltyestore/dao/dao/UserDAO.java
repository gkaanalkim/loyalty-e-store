package com.kgteknoloji.loyaltyestore.dao.dao;

import com.kgteknoloji.loyaltyestore.dao.core.BaseDAO;
import com.kgteknoloji.loyaltyestore.representation.entities.User;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;

import java.util.List;

public class UserDAO extends BaseDAO<User> {
    public UserDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public User findByEmail(String email, String password) {
        Criteria criteria = currentSession().createCriteria(User.class);
        criteria.add(Restrictions.eq("email", email)).add(Restrictions.eq("password", password)).add(Restrictions.eq("deleteStatus", false));
        return uniqueResult(criteria);
    }

    public List<User> getByNameContains(String name) {
        Criteria criteria = currentSession().createCriteria(User.class);
        criteria.add(Restrictions.like("name", name, MatchMode.ANYWHERE)).add(Restrictions.eq("deleteStatus", false));

        return list(criteria);
    }

    public User getByCode(String code) {
        Criteria criteria = currentSession().createCriteria(User.class);
        criteria.add(Restrictions.eq("deleteStatus", false)).add(Restrictions.eq("code", code));

        return uniqueResult(criteria);
    }

    public User getByEmail(String email) {
        Criteria criteria = currentSession().createCriteria(User.class);
        criteria.add(Restrictions.eq("deleteStatus", false)).add(Restrictions.eq("email", email));

        return uniqueResult(criteria);
    }

    public User findByUsername(String username) {
        Criteria criteria =currentSession().createCriteria(User.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("username", username));

        return uniqueResult(criteria);
    }

    public User findByOid(String oid) {
        Criteria criteria =currentSession().createCriteria(User.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("oid", oid));

        return uniqueResult(criteria);
    }

    public User findAll() {
        Criteria criteria =currentSession().createCriteria(User.class);
        criteria.add(Restrictions.eq("deleteStatus", false));

        return uniqueResult(criteria);
    }
}

