package com.kgteknoloji.loyaltyestore.dao.dao;

import com.kgteknoloji.loyaltyestore.dao.core.BaseDAO;
import com.kgteknoloji.loyaltyestore.representation.entities.TransactionType;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;

public class TransactionTypeDAO extends BaseDAO<TransactionType> {
    public TransactionTypeDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public TransactionType findByCode(String code) {
        Criteria criteria = currentSession().createCriteria(TransactionType.class);
        criteria.add(Restrictions.eq("deleteStatus", false));
        criteria.add(Restrictions.eq("code", code));

        return uniqueResult(criteria);
    }
}
