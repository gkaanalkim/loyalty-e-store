package com.kgteknoloji.loyaltyestore.dao.dao;

import com.kgteknoloji.loyaltyestore.dao.core.SoftDAO;
import com.kgteknoloji.loyaltyestore.representation.entities.CategoryAttribute;
import org.hibernate.SessionFactory;

public class CategoryAttributeDAO extends SoftDAO<CategoryAttribute> {
    public CategoryAttributeDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }
}
