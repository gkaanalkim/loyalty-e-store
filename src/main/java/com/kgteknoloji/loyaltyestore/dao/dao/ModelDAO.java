package com.kgteknoloji.loyaltyestore.dao.dao;

import com.kgteknoloji.loyaltyestore.dao.core.SoftDAO;
import com.kgteknoloji.loyaltyestore.representation.entities.Model;
import org.hibernate.SessionFactory;

public class ModelDAO extends SoftDAO<Model> {
    public ModelDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }
}
