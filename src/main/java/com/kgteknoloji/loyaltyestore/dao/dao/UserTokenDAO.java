package com.kgteknoloji.loyaltyestore.dao.dao;

import com.kgteknoloji.loyaltyestore.dao.core.BaseDAO;
import com.kgteknoloji.loyaltyestore.representation.entities.User;
import com.kgteknoloji.loyaltyestore.representation.entities.UserToken;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import java.util.List;

public class UserTokenDAO extends BaseDAO<UserToken> {
    public UserTokenDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public UserToken findByToken(String token) {
        Criteria criteria = currentSession().createCriteria(UserToken.class);
        criteria.add(Restrictions.eq("token", token));

        return uniqueResult(criteria);
    }

    public List<UserToken> findByCreationDate() {
        Criteria criteria = currentSession().createCriteria(UserToken.class);
        criteria.addOrder(Order.desc("creationDateTime")).setMaxResults(100);

        return list(criteria);
    }

    public UserToken getByUser(User user) {
        Criteria criteria = currentSession().createCriteria(UserToken.class);
        criteria.add(Restrictions.eq("user.oid", user.getOid()));

        return uniqueResult(criteria);
    }
}
