package com.kgteknoloji.loyaltyestore.dao.dao;

import com.kgteknoloji.loyaltyestore.dao.core.SoftDAO;
import com.kgteknoloji.loyaltyestore.representation.entities.Stock;
import org.hibernate.SessionFactory;

public class StockDAO extends SoftDAO<Stock> {
    public StockDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }
}
