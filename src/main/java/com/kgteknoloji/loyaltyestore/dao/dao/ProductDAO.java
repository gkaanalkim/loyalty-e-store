package com.kgteknoloji.loyaltyestore.dao.dao;

import com.kgteknoloji.loyaltyestore.dao.core.SoftDAO;
import com.kgteknoloji.loyaltyestore.representation.entities.Category;
import com.kgteknoloji.loyaltyestore.representation.entities.Product;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class ProductDAO extends SoftDAO<Product> {
    private static final Logger LOGGER = LoggerFactory.getLogger(ProductDAO.class);

    public ProductDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public List<Product> findByCategory(Category category) {
        Criteria criteria = currentSession().createCriteria(Product.class);
        criteria.add(Restrictions.eq("deleteStatus", false));
        criteria.add(Restrictions.eq("category.oid", category.getOid()));

        return criteria.list();
    }
}
