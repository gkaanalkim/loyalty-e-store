package com.kgteknoloji.loyaltyestore.dao.dao;

import com.kgteknoloji.loyaltyestore.dao.core.SoftDAO;
import com.kgteknoloji.loyaltyestore.representation.entities.Product;
import com.kgteknoloji.loyaltyestore.representation.entities.ProductVariation;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;

import java.util.List;

public class ProductVariationDAO extends SoftDAO<ProductVariation> {
    public ProductVariationDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public List<ProductVariation> findByProduct(Product product) {
        Criteria criteria = currentSession().createCriteria(ProductVariation.class);
        criteria.add(Restrictions.eq("deleteStatus", false));
        criteria.add(Restrictions.eq("product.oid",product.getOid()));

        return criteria.list();
    }
}
