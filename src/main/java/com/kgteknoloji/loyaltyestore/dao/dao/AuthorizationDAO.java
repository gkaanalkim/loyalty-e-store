package com.kgteknoloji.loyaltyestore.dao.dao;

import com.kgteknoloji.loyaltyestore.dao.core.BaseDAO;
import com.kgteknoloji.loyaltyestore.representation.entities.Authorization;
import com.kgteknoloji.loyaltyestore.representation.entities.Resource;
import com.kgteknoloji.loyaltyestore.representation.entities.Role;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;

import java.util.List;

public class AuthorizationDAO extends BaseDAO<Authorization> {
    public AuthorizationDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public List<Authorization> getByRole(Role role) {
        Criteria criteria = currentSession().createCriteria(Authorization.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("role.oid", role.getOid()));

        return list(criteria);
    }

    public Authorization findByRoleAndResource(Role role, Resource resource){
        Criteria criteria = currentSession().createCriteria(Authorization.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("role.oid", role.getOid()))
                .add(Restrictions.eq("resource.oid", resource.getOid()));

        return uniqueResult(criteria);
    }
}
