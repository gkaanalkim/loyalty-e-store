package com.kgteknoloji.loyaltyestore.dao.dao;

import com.kgteknoloji.loyaltyestore.dao.core.SoftDAO;
import com.kgteknoloji.loyaltyestore.representation.entities.Attribute;
import com.kgteknoloji.loyaltyestore.representation.entities.Product;
import com.kgteknoloji.loyaltyestore.representation.entities.ProductAttribute;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Map;

public class ProductAttributeDAO extends SoftDAO<ProductAttribute> {
    private static final Logger LOGGER = LoggerFactory.getLogger(ProductAttributeDAO.class);

    public ProductAttributeDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public List<ProductAttribute> findByProduct(Product product) {
        Criteria criteria = currentSession().createCriteria(ProductAttribute.class);
        criteria.add(Restrictions.eq("deleteStatus", false));
        criteria.add(Restrictions.eq("product.oid", product.getOid()));

        return criteria.list();
    }

    public List<Product> findByAttributes(Map<Attribute, String> attributeStringMap) {
        Criteria criteria = currentSession().createCriteria(ProductAttribute.class);
        criteria.createAlias("attribute", "attribute");

        for (Attribute attribute : attributeStringMap.keySet()) {
            LOGGER.info("Attribute Code : {}", attribute.getCode());

            criteria.add(Restrictions.eq("attribute.oid", attribute.getOid()))
                    .add(Restrictions.eq("attribute.value", attributeStringMap.get(attribute)));
        }

        criteria.setProjection(Projections.property("product"));

        return criteria.list();
    }
}
