package com.kgteknoloji.loyaltyestore.dao.dao;

import com.kgteknoloji.loyaltyestore.dao.core.BaseDAO;
import com.kgteknoloji.loyaltyestore.representation.entities.Balance;
import com.kgteknoloji.loyaltyestore.representation.entities.User;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;

public class BalanceDAO extends BaseDAO<Balance> {
    public BalanceDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public Balance findByUser(User user) {
        Criteria criteria = currentSession().createCriteria(Balance.class);
        criteria.add(Restrictions.eq("deleteStatus", false));
        criteria.add(Restrictions.eq("user.oid", user.getOid()));

        return uniqueResult(criteria);
    }
}
