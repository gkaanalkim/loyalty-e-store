package com.kgteknoloji.loyaltyestore.dao.dao;

import com.kgteknoloji.loyaltyestore.dao.core.SoftDAO;
import com.kgteknoloji.loyaltyestore.representation.entities.Price;
import com.kgteknoloji.loyaltyestore.representation.entities.Product;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;

import java.util.List;

public class PriceDAO extends SoftDAO<Price> {
    public PriceDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public List<Price> findByProduct(Product product) {
        Criteria criteria = currentSession().createCriteria(Price.class);
        criteria.add(Restrictions.eq("deleteStatus", false));
        criteria.add(Restrictions.eq("product.oid", product.getOid()));

        return criteria.list();
    }
}
