package com.kgteknoloji.loyaltyestore.dao.dao;

import com.kgteknoloji.loyaltyestore.dao.core.SoftDAO;
import com.kgteknoloji.loyaltyestore.representation.entities.Attribute;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;

public class AttributeDAO extends SoftDAO<Attribute> {
    public AttributeDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public Attribute findByCode(String code) {
        Criteria criteria = currentSession().createCriteria(Attribute.class);
        criteria.add(Restrictions.eq("deleteStatus", false));
        criteria.add(Restrictions.eq("code", code));

        return uniqueResult(criteria);
    }
}
