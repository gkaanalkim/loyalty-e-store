package com.kgteknoloji.loyaltyestore.dao.dao;

import com.kgteknoloji.loyaltyestore.dao.core.SoftDAO;
import com.kgteknoloji.loyaltyestore.representation.entities.Brand;
import org.hibernate.SessionFactory;

public class BrandDAO extends SoftDAO<Brand> {
    public BrandDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }
}
