package com.kgteknoloji.loyaltyestore.dao.dao;

import com.kgteknoloji.loyaltyestore.dao.core.SoftDAO;
import com.kgteknoloji.loyaltyestore.representation.entities.Unit;
import org.hibernate.SessionFactory;

public class UnitDAO extends SoftDAO<Unit> {
    public UnitDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }
}
