package com.kgteknoloji.loyaltyestore.dao.dao;

import com.kgteknoloji.loyaltyestore.dao.core.BaseDAO;
import com.kgteknoloji.loyaltyestore.representation.entities.Menu;
import com.kgteknoloji.loyaltyestore.representation.entities.Role;
import com.kgteknoloji.loyaltyestore.representation.entities.RoleMenu;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import java.util.List;

public class RoleMenuDAO extends BaseDAO<RoleMenu> {
    public RoleMenuDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public List<RoleMenu> getByRole(Role role) {
        Criteria criteria = currentSession().createCriteria(RoleMenu.class);
        criteria.createAlias("menu", "m");
        criteria.add(Restrictions.eq("role.oid", role.getOid()))
                .add(Restrictions.eq("deleteStatus", false))
                .addOrder(Order.asc("m.menuOrder"));
        return list(criteria);
    }

    public RoleMenu getByRoleAndMenu(Role role, Menu menu) {
        Criteria criteria = currentSession().createCriteria(RoleMenu.class);
        criteria.createAlias("menu", "m");

        criteria.add(Restrictions.eq("role.oid", role.getOid()))
                .add(Restrictions.eq("menu.oid", menu.getOid()));


        return uniqueResult(criteria);
    }

    public List<RoleMenu> getAll(Class<RoleMenu> t) {
        Criteria criteria = currentSession().createCriteria(t);
        criteria.add(Restrictions.eq("deleteStatus", false));

        return list(criteria);
    }

    public RoleMenu getById(String oid) {
        Criteria criteria = currentSession().createCriteria(RoleMenu.class);
        criteria.add(Restrictions.eq("deleteStatus", false)).add(Restrictions.eq("oid", oid));

        return uniqueResult(criteria);
    }
}
