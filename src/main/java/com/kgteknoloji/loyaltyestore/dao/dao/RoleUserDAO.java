package com.kgteknoloji.loyaltyestore.dao.dao;

import com.kgteknoloji.loyaltyestore.dao.core.BaseDAO;
import com.kgteknoloji.loyaltyestore.representation.entities.RoleUser;
import com.kgteknoloji.loyaltyestore.representation.entities.User;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;

import java.util.List;

public class RoleUserDAO extends BaseDAO<RoleUser> {
    public RoleUserDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public RoleUser findByUser(User user) {
        Criteria criteria = currentSession().createCriteria(RoleUser.class);
        criteria.add(Restrictions.eq("user.oid", user.getOid())).add(Restrictions.eq("deleteStatus", false));

        return uniqueResult(criteria);
    }

    @Override
    public List<RoleUser> findAll(Class<RoleUser> t) {
        Criteria criteria = currentSession().createCriteria(t);
        criteria.add(Restrictions.eq("deleteStatus", false));

        return list(criteria);
    }

    @Override
    public RoleUser findById(String oid) {
        Criteria criteria = currentSession().createCriteria(RoleUser.class);
        criteria.add(Restrictions.eq("deleteStatus", false)).add(Restrictions.eq("oid", oid));

        return uniqueResult(criteria);
    }

    @Override
    public RoleUser delete(RoleUser entity) {
        entity.setDeleteStatus(true);

        return super.persist(entity);
    }

    public RoleUser getByUser(User user) {
        Criteria criteria = currentSession().createCriteria(RoleUser.class);
        criteria.add(Restrictions.eq("user.oid", user.getOid()))
                .add(Restrictions.eq("deleteStatus", false));

        return uniqueResult(criteria);
    }
}
