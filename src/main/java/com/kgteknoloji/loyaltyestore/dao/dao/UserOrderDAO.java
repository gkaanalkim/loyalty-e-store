package com.kgteknoloji.loyaltyestore.dao.dao;

import com.kgteknoloji.loyaltyestore.dao.core.SoftDAO;
import com.kgteknoloji.loyaltyestore.representation.entities.UserOrder;
import org.hibernate.SessionFactory;

public class UserOrderDAO extends SoftDAO<UserOrder> {
    public UserOrderDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }
}
