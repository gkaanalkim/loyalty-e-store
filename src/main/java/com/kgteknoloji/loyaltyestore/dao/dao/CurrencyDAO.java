package com.kgteknoloji.loyaltyestore.dao.dao;

import com.kgteknoloji.loyaltyestore.dao.core.SoftDAO;
import com.kgteknoloji.loyaltyestore.representation.entities.Currency;
import org.hibernate.SessionFactory;

public class CurrencyDAO extends SoftDAO<Currency> {
    public CurrencyDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }
}
