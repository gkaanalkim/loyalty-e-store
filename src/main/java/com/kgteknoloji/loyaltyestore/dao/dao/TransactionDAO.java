package com.kgteknoloji.loyaltyestore.dao.dao;

import com.kgteknoloji.loyaltyestore.dao.core.BaseDAO;
import com.kgteknoloji.loyaltyestore.representation.entities.Transaction;
import org.hibernate.SessionFactory;

public class TransactionDAO extends BaseDAO<Transaction> {
    public TransactionDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }
}
