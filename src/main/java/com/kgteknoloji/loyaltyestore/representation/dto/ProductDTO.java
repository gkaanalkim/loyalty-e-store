package com.kgteknoloji.loyaltyestore.representation.dto;

import com.kgteknoloji.loyaltyestore.representation.entities.Price;
import com.kgteknoloji.loyaltyestore.representation.entities.Product;
import com.kgteknoloji.loyaltyestore.representation.entities.ProductAttribute;
import com.kgteknoloji.loyaltyestore.representation.entities.ProductVariation;

import java.util.List;

public class ProductDTO {
    private Product product;
    private List<Price> prices;
    private List<ProductAttribute> productAttributes;
    private List<ProductVariation> productVariations;

    public ProductDTO(Product product, List<Price> prices, List<ProductAttribute> productAttributes, List<ProductVariation> productVariations) {
        this.product = product;
        this.prices = prices;
        this.productAttributes = productAttributes;
        this.productVariations = productVariations;
    }

    public List<Price> getPrices() {
        return prices;
    }

    public void setPrices(List<Price> prices) {
        this.prices = prices;
    }

    public List<ProductAttribute> getProductAttributes() {
        return productAttributes;
    }

    public void setProductAttributes(List<ProductAttribute> productAttributes) {
        this.productAttributes = productAttributes;
    }

    public List<ProductVariation> getProductVariations() {
        return productVariations;
    }

    public void setProductVariations(List<ProductVariation> productVariations) {
        this.productVariations = productVariations;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }
}
