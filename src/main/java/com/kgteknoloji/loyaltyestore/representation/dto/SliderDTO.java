package com.kgteknoloji.loyaltyestore.representation.dto;

import java.util.List;

public class SliderDTO {
    private String pageName;
    private List<String> imagePaths;

    public String getPageName() {
        return pageName;
    }

    public void setPageName(String pageName) {
        this.pageName = pageName;
    }

    public List<String> getImagePaths() {
        return imagePaths;
    }

    public void setImagePaths(List<String> imagePaths) {
        this.imagePaths = imagePaths;
    }
}
