package com.kgteknoloji.loyaltyestore.representation.dto;

public class OrderDTO {
    private String productOid;
    private String productVariationOid;
    private String priceOid;
    private Integer quantity;

    public String getProductOid() {
        return productOid;
    }

    public void setProductOid(String productOid) {
        this.productOid = productOid;
    }

    public String getProductVariationOid() {
        return productVariationOid;
    }

    public void setProductVariationOid(String productVariationOid) {
        this.productVariationOid = productVariationOid;
    }

    public String getPriceOid() {
        return priceOid;
    }

    public void setPriceOid(String priceOid) {
        this.priceOid = priceOid;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }
}
