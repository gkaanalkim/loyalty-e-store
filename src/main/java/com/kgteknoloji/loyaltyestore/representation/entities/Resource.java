package com.kgteknoloji.loyaltyestore.representation.entities;

import com.kgteknoloji.loyaltyestore.representation.core.BaseEntity;

import javax.persistence.Entity;

@Entity
public class Resource extends BaseEntity {
    private String name;
    private String alias;
    private String description;
    private String author;
    private String path;
    private String version;

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }
}
