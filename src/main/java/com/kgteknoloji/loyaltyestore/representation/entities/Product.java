package com.kgteknoloji.loyaltyestore.representation.entities;

import com.kgteknoloji.loyaltyestore.representation.core.BaseEntity;
import org.hibernate.annotations.Type;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Product extends BaseEntity {
    private String name;
    private String code;
    private String sku;
    private String imagePaths;

    @Type(type = "text")
    private String description;

    @ManyToOne
    @JoinColumn(name = "categoryOid", referencedColumnName = "oid")
    private Category category;

    @ManyToOne
    @JoinColumn(name = "brandOid", referencedColumnName = "oid")
    private Brand brand;

    @ManyToOne
    @JoinColumn(name = "modelOid", referencedColumnName = "oid")
    private Model model;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public String getImagePaths() {
        return imagePaths;
    }

    public void setImagePaths(String imagePaths) {
        this.imagePaths = imagePaths;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Brand getBrand() {
        return brand;
    }

    public void setBrand(Brand brand) {
        this.brand = brand;
    }

    public Model getModel() {
        return model;
    }

    public void setModel(Model model) {
        this.model = model;
    }
}
