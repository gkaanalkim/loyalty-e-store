package com.kgteknoloji.loyaltyestore.representation.entities;

import com.kgteknoloji.loyaltyestore.representation.core.BaseEntity;

import javax.persistence.*;

@Entity
public class ProductAttribute extends BaseEntity {
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "productOid", nullable = false)
    private Product product;

    @ManyToOne
    @JoinColumn(name = "attributeOid", referencedColumnName = "oid")
    private Attribute attribute;

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Attribute getAttribute() {
        return attribute;
    }

    public void setAttribute(Attribute attribute) {
        this.attribute = attribute;
    }
}
