package com.kgteknoloji.loyaltyestore.representation.entities;

import com.kgteknoloji.loyaltyestore.representation.core.BaseEntity;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class CategoryAttribute extends BaseEntity {
    @ManyToOne
    @JoinColumn(name = "categoryOid", referencedColumnName = "oid")
    private Category category;

    @ManyToOne
    @JoinColumn(name = "attributeOid", referencedColumnName = "oid")
    private Attribute attribute;

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Attribute getAttribute() {
        return attribute;
    }

    public void setAttribute(Attribute attribute) {
        this.attribute = attribute;
    }
}
