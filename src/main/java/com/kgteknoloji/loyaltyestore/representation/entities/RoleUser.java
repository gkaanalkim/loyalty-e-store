package com.kgteknoloji.loyaltyestore.representation.entities;

import com.kgteknoloji.loyaltyestore.representation.core.BaseEntity;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class RoleUser extends BaseEntity {

    @ManyToOne
    @JoinColumn(name = "userOid", referencedColumnName = "oid")
    private User user;

    @ManyToOne
    @JoinColumn(name = "roleOid", referencedColumnName = "oid")
    private Role role;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }
}
