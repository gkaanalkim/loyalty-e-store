package com.kgteknoloji.loyaltyestore.representation.entities;

import com.kgteknoloji.loyaltyestore.representation.core.BaseEntity;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
public class Price extends BaseEntity {
    @ManyToOne
    @JoinColumn(name = "productOid")
    private Product product;

    @OneToOne
    @JoinColumn(name = "currencyOid")
    private Currency currency;

    private String name;
    private BigDecimal value;

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }
}
