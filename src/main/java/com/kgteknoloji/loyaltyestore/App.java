package com.kgteknoloji.loyaltyestore;

import com.kgteknoloji.loyaltyestore.authentication.WandAuthenticationBundle;
import com.kgteknoloji.loyaltyestore.authorization.WandAuthorizationBundle;
import com.kgteknoloji.loyaltyestore.cli.InitializeCommand;
import com.kgteknoloji.loyaltyestore.dao.dao.*;
import com.kgteknoloji.loyaltyestore.representation.entities.*;
import com.kgteknoloji.loyaltyestore.resource.*;
import com.kgteknoloji.loyaltyestore.wandservice.WandResourceBundle;
import io.dropwizard.Application;
import io.dropwizard.db.DataSourceFactory;
import io.dropwizard.hibernate.HibernateBundle;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import org.eclipse.jetty.servlets.CrossOriginFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.DispatcherType;
import javax.servlet.FilterRegistration;
import java.util.Arrays;
import java.util.EnumSet;
import java.util.List;

public class App extends Application<BaseConfiguration> {
    private static final Logger LOGGER = LoggerFactory.getLogger(App.class);

    private final HibernateBundle<BaseConfiguration> hibernateBundle = new HibernateBundle<BaseConfiguration>(
            Menu.class,
            Role.class,
            User.class,
            RoleMenu.class,
            RoleUser.class,
            UserToken.class,
            Resource.class,
            Authorization.class,
            /**
             * Application Entities
             * **/
            Category.class,
            Brand.class,
            Model.class,
            Product.class,
            Currency.class,
            Product.class,
            ProductAttribute.class,
            ProductVariation.class,
            Unit.class,
            Stock.class,
            Price.class,
            UserOrder.class,
            Shipment.class,
            Transaction.class,
            TransactionType.class,
            Balance.class,
            Slider.class,
            Attribute.class,
            CategoryAttribute.class
    ) {
        @Override
        public DataSourceFactory getDataSourceFactory(BaseConfiguration baseConfiguration) {
            return baseConfiguration.getDatabase();
        }
    };

    private List wandServiceClasses = Arrays.asList(
            AuthResource.class,
            MenuResource.class,
            RoleResource.class,
            UserResource.class,
            ServiceResource.class,
            BrandResource.class,
            CategoryResource.class,
            ModelResource.class,
            UnitResource.class,
            CurrencyResource.class,
            UserOrderResource.class
    );

    @Override
    public void initialize(Bootstrap<BaseConfiguration> bootstrap) {
        bootstrap.addBundle(this.hibernateBundle);
        bootstrap.addCommand(new InitializeCommand<>(this, hibernateBundle));

        bootstrap.addBundle(new WandAuthenticationBundle<BaseConfiguration>(hibernateBundle));
        bootstrap.addBundle(new WandAuthorizationBundle<BaseConfiguration>(hibernateBundle));
        bootstrap.addBundle(new WandResourceBundle<BaseConfiguration>(hibernateBundle, wandServiceClasses));
    }

    @Override
    public void run(BaseConfiguration configuration, Environment environment) throws Exception {
        LOGGER.info("Services are run.");

        /**
         * CORS Settings
         * **/
        FilterRegistration.Dynamic filter = environment.servlets().addFilter("CORS", CrossOriginFilter.class);
        filter.setInitParameter(CrossOriginFilter.ALLOWED_METHODS_PARAM, "GET,PUT,POST,DELETE,OPTIONS");
        filter.setInitParameter(CrossOriginFilter.ALLOWED_ORIGINS_PARAM, "*");
        filter.setInitParameter(CrossOriginFilter.ACCESS_CONTROL_ALLOW_ORIGIN_HEADER, "*");
        filter.setInitParameter(CrossOriginFilter.ALLOWED_HEADERS_PARAM, "Content-Type,Authorization,X-Requested-With,Content-Length,Accept,Origin");
        filter.addMappingForUrlPatterns(EnumSet.allOf(DispatcherType.class), true, "/*");

        /**
         * System DAOs
         * **/
        final RoleDAO roleDAO = new RoleDAO(hibernateBundle.getSessionFactory());
        final RoleMenuDAO roleMenuDAO = new RoleMenuDAO(hibernateBundle.getSessionFactory());
        final RoleUserDAO roleUserDAO = new RoleUserDAO(hibernateBundle.getSessionFactory());
        final UserDAO userDAO = new UserDAO(hibernateBundle.getSessionFactory());
        final UserTokenDAO userTokenDAO = new UserTokenDAO(hibernateBundle.getSessionFactory());
        final MenuDAO menuDAO = new MenuDAO(hibernateBundle.getSessionFactory());
        final AuthorizationDAO authorizationDAO = new AuthorizationDAO(hibernateBundle.getSessionFactory());
        final ResourceDAO resourceDAO = new ResourceDAO(hibernateBundle.getSessionFactory());

        /**
         * System Resources
         * **/
        environment.jersey().register(new AuthResource(userDAO, userTokenDAO));
        environment.jersey().register(new MenuResource(userDAO, menuDAO, roleDAO, roleMenuDAO, roleUserDAO));
        environment.jersey().register(new RoleResource(roleDAO, userDAO, roleUserDAO));
        environment.jersey().register(new ServiceResource(resourceDAO, roleDAO, authorizationDAO));

        /**
         * Application DAOs
         * **/
        final BrandDAO brandDAO = new BrandDAO(hibernateBundle.getSessionFactory());
        final CategoryDAO categoryDAO = new CategoryDAO(hibernateBundle.getSessionFactory());
        final UnitDAO unitDAO = new UnitDAO(hibernateBundle.getSessionFactory());
        final CurrencyDAO currencyDAO = new CurrencyDAO(hibernateBundle.getSessionFactory());
        final ModelDAO modelDAO = new ModelDAO(hibernateBundle.getSessionFactory());
        final PriceDAO priceDAO = new PriceDAO(hibernateBundle.getSessionFactory());
        final ProductDAO productDAO = new ProductDAO(hibernateBundle.getSessionFactory());
        final ProductAttributeDAO productAttributeDAO = new ProductAttributeDAO(hibernateBundle.getSessionFactory());
        final ProductVariationDAO productVariationDAO = new ProductVariationDAO(hibernateBundle.getSessionFactory());
        final UserOrderDAO userOrderDAO = new UserOrderDAO(hibernateBundle.getSessionFactory());
        final BalanceDAO balanceDAO = new BalanceDAO(hibernateBundle.getSessionFactory());
        final TransactionDAO transactionDAO = new TransactionDAO(hibernateBundle.getSessionFactory());
        final TransactionTypeDAO transactionTypeDAO = new TransactionTypeDAO(hibernateBundle.getSessionFactory());
        final SliderDAO sliderDAO = new SliderDAO(hibernateBundle.getSessionFactory());
        final AttributeDAO attributeDAO = new AttributeDAO(hibernateBundle.getSessionFactory());
        final CategoryAttributeDAO categoryAttributeDAO = new CategoryAttributeDAO(hibernateBundle.getSessionFactory());

        /**
         * Application Resources
         * **/
        environment.jersey().register(new BrandResource(brandDAO));
        environment.jersey().register(new CategoryResource(categoryDAO, productDAO));
        environment.jersey().register(new UnitResource(unitDAO));
        environment.jersey().register(new CurrencyResource(currencyDAO));
        environment.jersey().register(new ModelResource(modelDAO));
        environment.jersey().register(new PriceResource(priceDAO));
        environment.jersey().register(new ProductResource(productDAO, productAttributeDAO, productVariationDAO, priceDAO, attributeDAO, categoryAttributeDAO));
        environment.jersey().register(new UserOrderResource(userDAO, productDAO, productVariationDAO, userOrderDAO, priceDAO, balanceDAO, transactionDAO, transactionTypeDAO));
        environment.jersey().register(new SliderResource(sliderDAO));
    }

    public static void main(String[] args) throws Exception {
        new App().run(args);
    }
}